const hre = require("hardhat");

async function main() {  
  const EnticeCoin = await hre.ethers.getContractFactory("EnticeCoin");
  const enticeCoin = await EnticeCoin.deploy();

  await enticeCoin.deployed();

  console.log(
    `Entice Coin deployed to ${enticeCoin.address}`
  );
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
